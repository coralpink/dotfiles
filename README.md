# dotfiles

> managing dotfiles has never been so ~~easy~~ convoluted!
>
> edit: we posix now

![screenshot](./screenshots/20250207_054608.png)

## what

my dotfiles generation rube goldberg contraption thingee

## which

- alacritty
- bash
- curl
- dunst
- i3
- lsd
- mpd
- mpv
- ncmpcpp
- neovim
- polybar
- rofi
- xorg

## how

```bash
git clone 'https://codeberg.org/coralpink/dotfiles' "$HOME"/.dotfiles
cd "$HOME"/.dotfiles
# edit ./config/*
make install
```

## may I

[sure](./LICENSE.md)

