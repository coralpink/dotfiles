#!/bin/sh
passage_store="$HOME"/.passage/store

password_get_icon() {
    password="${1:-}"
    extensions='bmp png jpg jpeg ico svg txt'

    # look up explicit icon
    # like /home/anon/.passage/store/Directory/Site.age.png
    for extension in $extensions; do
        if [ -f "${password}.${extension}" ]; then
            if [ "${extension}" = 'txt' ]; then
                cat "${password}.${extension}"
                return
            else
                echo "${password}.${extension}"
                return
            fi
        fi
    done

    # look up directory icon
    # recursing up to root directory
    # like /home/anon/.passage/store/Directory/icon.png
    path="$password"
    while ! [ "$path" = "$passage_store" ]; do
        path="${path%/*}"

        for extension in $extensions; do
            if [ -f "${path}/icon.${extension}" ]; then
                if [ "${extension}" = 'txt' ]; then
                    cat "${path}/icon.${extension}"
                    return
                else
                    echo "${path}/icon.${extension}"
                    return
                fi
            fi
        done
    done

    # no icon
    echo ''
}

# hopefully no newlines in filenames, lmao
passwords=$(find "$passage_store" -type f -name '*.age')

selected=$(echo "$passwords" | while read -r password; do
    password_fullname=$(echo "$password" | sed -e 's|^'"$passage_store"/'||;s|\.age$||')
    password_shortname=${password_fullname##*/}
    password_path=${password_fullname%/*}
    password_icon=$(password_get_icon "$password")

    printf '%s <i>(%s)</i>\0icon\x1f%s\n' "$password_shortname" "$password_path" "$password_icon"
done | rofi -dmenu -p 'password' -format 'd' -i -markup-rows -show-icons -no-custom)

if [ -n "$selected" ]; then
    password=$(echo "$passwords" | sed -ne "$selected"'{;p;q}')
    password_fullname=$(echo "$password" | sed -e 's|^'"$passage_store"'/||;s|\.age$||')
    password_shortname=${password_fullname##*/}
    password_icon=$(password_get_icon "$password")

    passage_log=$(PASSAGE_AGE=rage passage show --clip "$password_fullname" 2>&1 | head -n 1)

    notify-send -i "$password_icon" "passage: $password_shortname" "$passage_log"
fi

