#!/bin/sh
sink="${SINK:-@DEFAULT_SINK@}"
interval="${INTERVAL:-5}"
increment="${INCREMENT:-5}"
label_offline="${LABEL_OFFLINE:-"(offline)"}"
label_muted="${LABEL_MUTED:-"(muted)"}"
label_error="${LABEL_ERROR:-"(error)"}"
current_volume="$label_offline"
pid="${2:-}"

signal_main_loop() {
    if [ -n "$pid" ]; then
        kill -USR1 "$pid"
    fi
}

change_volume() {
    wpctl set-volume -l 1.0 "$sink" "$1" 2> /dev/null
    signal_main_loop
}

toggle_mute() {
    wpctl set-mute "$sink" toggle 2> /dev/null
    signal_main_loop
}

get_volume() {
    if ! volume=$(wpctl get-volume "$sink" 2> /dev/null); then
        echo "$label_offline"
        return 1
    fi

    if ! echo "$volume" | grep -q '^Volume: '; then
        echo "$label_error"
        return 1
    fi

    volume=$(echo "$volume" | sed -e 's/^Volume: //')

    if echo "$volume" | grep -q '\[MUTED\]$'; then
        echo "$label_muted"
        return 0
    fi

    volume=$(echo "$volume * 100/1" | bc 2> /dev/null)

    echo "${volume}%"
    return 0
}

update_volume() {
    current_volume="$(get_volume)"
    rc=$?

    echo "$current_volume"
    return $rc
}

run_main_loop() {
    while true; do
        update_volume

        sleep "$interval" &
        sleep_pid=$!
        wait $sleep_pid
    done
}



case "${1:-}" in
    increase)
        change_volume "${increment}%+"
        ;;

    decrease)
        change_volume "${increment}%-"
        ;;

    mute)
        toggle_mute
        ;;

    '')
        trap update_volume USR1
        run_main_loop
        ;;

    *)
        exit 69
        ;;
esac

