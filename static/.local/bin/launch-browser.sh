#!/bin/sh
# shellcheck disable=SC2153
prefix() {
    case "$1" in
        firefox) echo "$HOME/.mozilla/firefox";;
        librewolf) echo "$HOME/.librewolf";;
        icecat) echo "$HOME/.mozilla/icecat";;
        *) exit 1;;
    esac
}

fullname() {
    case "$1" in
        firefox) echo "Mozilla Firefox";;
        librewolf) echo "LibreWolf";;
        icecat) echo "GNU IceCat";;
        *) exit 1;;
    esac
}

browsers='firefox librewolf icecat'

profiles=$(for browser in $browsers; do
    ini="$(prefix "$browser")/profiles.ini"

    if [ -f "$ini" ]; then
        sed -ne 's/^Name=//;t1;b;:1;h;:2;n;s/^Path=//;t3;b2;:3;H;g;s/\n/|/;s/^/'"$browser"'|/p' "$ini"
    fi
done)

marked="$(echo "$profiles" | sed -ne '/default/=' | while read -r line; do
    echo $((line - 1))
done | tr '\n' ',')"

selected=$(echo "$profiles" | while IFS='|' read -r browser name path; do
    printf '%s - %s <i>(%s)</i> \0icon\x1f%s\n' "$(fullname "$browser")" "$name" "$path" "$browser"
done | rofi -dmenu -p 'profile' -format 'd' -a "$marked" -select "$BROWSER default" -i -markup-rows -show-icons -no-custom)

if [ -n "$selected" ]; then
    echo "$profiles" | sed -ne ''"$selected"'{p;q}' | {
        IFS='|' read -r browser _ path
        "$browser" --profile "$(prefix "$browser")/$path"
    }
fi

