#!/bin/sh

export APPLICATIONS_TERMINAL="${APPLICATIONS_TERMINAL:-alacritty}"
export APPLICATIONS_IMAGEVIEWER="${APPLICATIONS_IMAGEVIEWER:-nsxiv}"
export APPLICATIONS_MUSICPLAYER="${APPLICATIONS_MUSICPLAYER:-mpv}"
export APPLICATIONS_VIDEOPLAYER="${APPLICATIONS_VIDEOPLAYER:-mpv}"
export APPLICATIONS_FILEMANAGER="${APPLICATIONS_FILEMANAGER:-pcmanfm}"
export APPLICATIONS_EDITOR="${APPLICATIONS_EDITOR:-nvim}"
export APPLICATIONS_WEBBROWSER="${APPLICATIONS_WEBBROWSER:-icecat}"
export APPLICATIONS_PDFVIEWER="${APPLICATIONS_PDFVIEWER:-icecat}"

