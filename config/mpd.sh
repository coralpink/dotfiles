#!/bin/sh

. ./user.sh

export MPD_ENABLED="${MPD_ENABLED:-1}"
export MPD_MUSICDIRECTORY="${MPD_MUSICDIRECTORY:-/srv/data/music}"
export MPD_DATADIRECTORY="$TARGET_HOME"/.local/state/mpd
export MPD_ADDRESS="$MPD_DATADIRECTORY"/socket

