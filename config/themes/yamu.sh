#!/bin/sh

export THEME_NAME=yamu

export THEME_BACKGROUND='#060447'
export THEME_FOREGROUND='#b9e37e'

export THEME_NORMAL_BLACK='#062376'
export THEME_NORMAL_RED='#de3c80'
export THEME_NORMAL_GREEN='#5ab57a'
export THEME_NORMAL_YELLOW='#f165a7'
export THEME_NORMAL_BLUE='#0b4996'
export THEME_NORMAL_MAGENTA='#6730db'
export THEME_NORMAL_CYAN='#068c8c'
export THEME_NORMAL_WHITE='#15caac'

export THEME_BRIGHT_BLACK='#039aa2'
export THEME_BRIGHT_RED='#f15c97'
export THEME_BRIGHT_GREEN='#89e67a'
export THEME_BRIGHT_YELLOW='#f69caa'
export THEME_BRIGHT_BLUE='#5e9da9'
export THEME_BRIGHT_MAGENTA='#f80aad'
export THEME_BRIGHT_CYAN='#8adea7'
export THEME_BRIGHT_WHITE='#2fe6a9'

export THEME_PRIMARY="$THEME_BRIGHT_BLACK"
export THEME_PRIMARY_VARIANT="$THEME_NORMAL_GREEN"
export THEME_ON_PRIMARY="$THEME_BRIGHT_GREEN"

export THEME_SECONDARY="$THEME_NORMAL_BLUE"
export THEME_SECONDARY_VARIANT="$THEME_BRIGHT_BLUE"
export THEME_ON_SECONDARY="$THEME_BRIGHT_WHITE"

export THEME_SURFACE="$THEME_NORMAL_BLACK"
export THEME_ON_SURFACE="$THEME_FOREGROUND"

export THEME_SUCCESS="$THEME_NORMAL_GREEN"
export THEME_ON_SUCCESS="$THEME_BRIGHT_WHITE"

export THEME_WARNING="$THEME_NORMAL_YELLOW"
export THEME_ON_WARNING="$THEME_BRIGHT_WHITE"

export THEME_ERROR="$THEME_NORMAL_RED"
export THEME_ON_ERROR="$THEME_BRIGHT_WHITE"

