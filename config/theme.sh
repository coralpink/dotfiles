#!/bin/sh

DEFAULT_THEME=tango

if [ -z "$THEME" ]; then
    notice "THEME not set, using default theme \"$DEFAULT_THEME\""
    THEME="$DEFAULT_THEME"
fi

if ! [ -f ./themes/"${THEME}".sh ]; then 
    error "theme \"$THEME\" doesn't exist"
fi

# shellcheck disable=SC1090
. ./themes/"${THEME}".sh

