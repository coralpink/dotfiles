#!/bin/sh

if [ -z "$TARGET_USER" ]; then
    notice "TARGET_USER not set, using \$USER ($USER)"

    TARGET_USER="$USER"
fi

if [ -z "$TARGET_HOME" ]; then
    notice "TARGET_HOME not set, using \$HOME ($HOME)"

    TARGET_HOME="$HOME"
fi

if [ -z "$TARGET_HOME" ]; then
    error "TARGET_HOME can't be empty"
fi

export USER="$TARGET_USER"
export HOME="$TARGET_HOME"

