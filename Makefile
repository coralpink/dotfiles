MAKEFLAGS += -rR

.POSIX:
.SUFFIXES:

config_directory := config
templates_directory := templates
static_directory := static
build_directory := build
dist_directory := dist

build := scripts/build.sh
postinstall := hooks/postinstall.sh

$(build_directory)/%: $(templates_directory)/%.sh
	@mkdir -p $(dir $@)
	$(build) $^ > $@

$(build_directory)/%: $(static_directory)/%
	@mkdir -p $(dir $@)
	cp $< $@

# config dependencies
$(config_directory)/theme.sh: $(config_directory)/themes/sign.sh
$(config_directory)/theme.sh: $(config_directory)/themes/tango.sh
$(config_directory)/theme.sh: $(config_directory)/themes/usagi.sh
$(config_directory)/theme.sh: $(config_directory)/themes/yamu.sh

$(config_directory)/mpd.sh: $(config_directory)/user.sh

# build dependencies
$(build_directory)/.config/i3/config: $(config_directory)/theme.sh
$(build_directory)/.config/i3/config: $(config_directory)/fonts.sh
$(build_directory)/.config/i3/config: $(config_directory)/applications.sh
$(build_directory)/.config/i3/config: $(config_directory)/mpd.sh

$(build_directory)/.config/alacritty/alacritty.toml: $(config_directory)/theme.sh
$(build_directory)/.config/alacritty/alacritty.toml: $(config_directory)/fonts.sh

$(build_directory)/.config/mpd/mpd.conf: $(config_directory)/mpd.sh
$(build_directory)/.config/ncmpcpp/config: $(config_directory)/mpd.sh

$(build_directory)/.profile: $(config_directory)/applications.sh
$(build_directory)/.profile: $(config_directory)/mpd.sh

$(build_directory)/.config/dunst/dunstrc: $(config_directory)/theme.sh
$(build_directory)/.config/dunst/dunstrc: $(config_directory)/fonts.sh

$(build_directory)/.gtkrc-2.0: $(config_directory)/gtk.sh
$(build_directory)/.gtkrc-2.0: $(config_directory)/fonts.sh

$(build_directory)/.config/gtk-3.0/settings.ini: $(config_directory)/gtk.sh
$(build_directory)/.config/gtk-3.0/settings.ini: $(config_directory)/fonts.sh

$(build_directory)/.config/rofi/themes/custom.rasi: $(config_directory)/theme.sh
$(build_directory)/.config/rofi/themes/custom.rasi: $(config_directory)/fonts.sh

$(build_directory)/.Xresources: $(config_directory)/theme.sh

$(build_directory)/.config/polybar/config.ini: $(config_directory)/theme.sh
$(build_directory)/.config/polybar/config.ini: $(config_directory)/fonts.sh

$(build_directory)/.config/nvim/init.lua: $(config_directory)/neovim.sh

$(build_directory)/.config/mimeapps.list: $(config_directory)/applications.sh

.PHONY: build dist install clean 
.PHONY: i3 alacritty mpd shell dunst gtk lsd mpv rofi xinit curl polybar neovim mimeapps misc

build: i3 alacritty mpd shell dunst gtk lsd mpv rofi xinit curl polybar neovim mimeapps misc

i3: $(build_directory)/.config/i3/config
alacritty: $(build_directory)/.config/alacritty/alacritty.toml
mpd: $(build_directory)/.config/mpd/mpd.conf $(build_directory)/.config/ncmpcpp/config
shell: $(build_directory)/.profile $(build_directory)/.bash_profile $(build_directory)/.bashrc
dunst: $(build_directory)/.config/dunst/dunstrc
gtk: $(build_directory)/.gtkrc-2.0 $(build_directory)/.config/gtk-3.0/settings.ini
lsd: $(build_directory)/.config/lsd/colors.yaml $(build_directory)/.config/lsd/config.yaml
mpv: $(build_directory)/.config/mpv/mpv.conf
rofi: $(build_directory)/.config/rofi/config.rasi $(build_directory)/.config/rofi/themes/custom.rasi
xinit: $(build_directory)/.xinitrc $(build_directory)/.Xresources
curl: $(build_directory)/.curlrc
polybar: $(build_directory)/.config/polybar/config.ini $(build_directory)/.local/bin/polybar_pipewire.sh
neovim: $(build_directory)/.config/nvim/init.lua
mimeapps: $(build_directory)/.config/mimeapps.list
misc: $(build_directory)/.local/bin/launch-browser.sh $(build_directory)/.local/bin/passage-menu.sh

dist: build
	@mkdir $(dist_directory)
	tar -czf $(dist_directory)/"dotfiles-$$(date --utc '+%Y%m%d%H%M%S').tar.gz" -C $(build_directory) .

install: build
	cp -r $(build_directory) -T $(HOME)
	$(postinstall)

clean:
	test -d $(dist_directory) && rm -r $(dist_directory) || true
	test -d $(build_directory) && rm -r $(build_directory) || true

