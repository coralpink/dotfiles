#!/bin/sh

cat << EOF
gtk-theme-name="$GTK_THEME"
gtk-icon-theme-name="$GTK_ICONS"
gtk-font-name="$FONTS_SANS_FAMILY, $FONTS_SANS_SIZE"
gtk-cursor-theme-name="$GTK_CURSOR"
gtk-cursor-theme-size=0
gtk-toolbar-style=GTK_TOOLBAR_ICONS
gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
gtk-button-images=1
gtk-menu-images=1
gtk-enable-event-sounds=1
gtk-enable-input-feedback-sounds=1
gtk-xft-antialias=1
gtk-xft-hinting=1
gtk-xft-hintstyle="hintfull"
EOF
