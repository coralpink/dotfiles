#!/bin/sh

cat << EOF
export PATH="\$PATH:\$HOME/.local/bin"
export TERMINAL=$APPLICATIONS_TERMINAL
export BROWSER=$APPLICATIONS_WEBBROWSER
export EDITOR=$APPLICATIONS_EDITOR
export VISUAL=$APPLICATIONS_EDITOR
EOF

[ -n "$MPD_ENABLED" ] && cat << EOF
export MPD_HOST=$MPD_ADDRESS
EOF

echo

