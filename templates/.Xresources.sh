#!/bin/sh

cat << EOF
Xft.autohint: 1
Xft.antialias: 1
Xft.hinting: true
Xft.hintstyle: hintslight
Xft.rgba: rgb
Xft.lcdfilter: lcddefault

! special
*.foreground:   $THEME_FOREGROUND
*.background:   $THEME_BACKGROUND
*.cursorColor:  $THEME_FOREGROUND

! black
*.color0:       $THEME_NORMAL_BLACK
*.color8:       $THEME_BRIGHT_BLACK

! red
*.color1:       $THEME_NORMAL_RED
*.color9:       $THEME_BRIGHT_RED

! green
*.color2:       $THEME_NORMAL_GREEN
*.color10:      $THEME_BRIGHT_GREEN

! yellow
*.color3:       $THEME_NORMAL_YELLOW
*.color11:      $THEME_BRIGHT_YELLOW

! blue
*.color4:       $THEME_NORMAL_BLUE
*.color12:      $THEME_BRIGHT_BLUE

! magenta
*.color5:       $THEME_NORMAL_MAGENTA
*.color13:      $THEME_BRIGHT_MAGENTA

! cyan
*.color6:       $THEME_NORMAL_CYAN
*.color14:      $THEME_BRIGHT_CYAN

! white
*.color7:       $THEME_NORMAL_WHITE
*.color15:      $THEME_BRIGHT_WHITE

EOF

