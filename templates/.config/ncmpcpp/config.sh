#!/bin/sh

cat << EOF
mpd_host = "$MPD_ADDRESS";

visualizer_data_source = "$MPD_DATADIRECTORY/audio.fifo";
visualizer_output_name = "fifo";
visualizer_in_stereo = "yes";
visualizer_look = "+|";
visualizer_type = "spectrum";

EOF

