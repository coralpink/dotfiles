#!/bin/sh

cat << EOF
set \$workspace1 1
set \$workspace2 2
set \$workspace3 3
set \$workspace4 4
set \$workspace5 5
set \$workspace6 6
set \$workspace7 7
set \$workspace8 8
set \$workspace9 9
set \$workspace0 10

set \$mod Mod4

set \$mode_resize resize
set \$mode_screenshot screenshot: [f]ull screen | [r]egion
EOF

[ -n "$MPD_ENABLED" ] && cat << EOF
set \$mode_mpd mpd: [p]lay/[p]ause | [s]top | [n]ext | p[r]evious
EOF

cat << EOF
set \$mode_volume volume: [+] up | [-] down | toggle [m]ute

font pango:$FONTS_MONOSPACE_FAMILY $FONTS_MONOSPACE_SIZE

floating_modifier \$mod
tiling_drag modifier titlebar

exec_always --no-startup-id "sh -c 'polybar-msg cmd quit; polybar top'"
exec_always --no-startup-id dunst
EOF

if [ -n "$WALLPAPER" ]; then cat << EOF
exec_always --no-startup-id "xwallpaper --zoom '$WALLPAPER'"
EOF
else cat << EOF
exec_always --no-startup-id "xsetroot -solid '$THEME_NORMAL_BLACK'"
EOF
fi

cat << EOF

bindsym \$mod+Return exec --no-startup-id "$APPLICATIONS_TERMINAL"
bindsym \$mod+Shift+Return exec --no-startup-id "$APPLICATIONS_TERMINAL --class floating"
bindsym \$mod+n exec --no-startup-id "$APPLICATIONS_TERMINAL --class floating --command ncmpcpp"
bindsym \$mod+p exec --no-startup-id "passage-menu.sh"

bindsym \$mod+e exec '$APPLICATIONS_FILEMANAGER'
bindsym \$mod+w exec '$APPLICATIONS_WEBBROWSER'
bindsym \$mod+Shift+w exec launch-browser.sh
bindsym \$mod+grave exec "rofi -modi drun,run -show drun -show-icons"
bindsym \$mod+semicolon exec "rofi -modi emoji -show emoji"

bindsym \$mod+h focus left
bindsym \$mod+j focus down
bindsym \$mod+k focus up
bindsym \$mod+l focus right

bindsym \$mod+Shift+h move left
bindsym \$mod+Shift+j move down
bindsym \$mod+Shift+k move up
bindsym \$mod+Shift+l move right

bindsym \$mod+c move position center

bindsym \$mod+Shift+minus split h
bindsym \$mod+Shift+backslash split v

bindsym \$mod+f fullscreen toggle
bindsym \$mod+s layout stacking
bindsym \$mod+t layout tabbed
bindsym \$mod+q layout toggle split

bindsym \$mod+space focus mode_toggle
bindsym \$mod+a focus parent
bindsym \$mod+Shift+space floating toggle

bindsym \$mod+Shift+q kill
bindsym \$mod+Shift+r restart
bindsym \$mod+Shift+Escape exec i3-msg exit

bindsym Print exec --no-startup-id "maim --select | xclip -selection clipboard -target image/png -in"

bindsym \$mod+r mode "\$mode_resize"
bindsym \$mod+Print mode "\$mode_screenshot"
bindsym \$mod+m mode "\$mode_volume"
EOF

[ -n "$MPD_ENABLED" ] && cat << EOF
bindsym \$mod+Shift+m mode "\$mode_mpd"
EOF

cat << EOF
bindsym \$mod+Tab exec "rofi -show window -show-icons"

bindsym \$mod+1 workspace number \$workspace1
bindsym \$mod+2 workspace number \$workspace2
bindsym \$mod+3 workspace number \$workspace3
bindsym \$mod+4 workspace number \$workspace4
bindsym \$mod+5 workspace number \$workspace5
bindsym \$mod+6 workspace number \$workspace6
bindsym \$mod+7 workspace number \$workspace7
bindsym \$mod+8 workspace number \$workspace8
bindsym \$mod+9 workspace number \$workspace9
bindsym \$mod+0 workspace number \$workspace0

bindsym \$mod+Shift+1 move container to workspace number \$workspace1
bindsym \$mod+Shift+2 move container to workspace number \$workspace2
bindsym \$mod+Shift+3 move container to workspace number \$workspace3
bindsym \$mod+Shift+4 move container to workspace number \$workspace4
bindsym \$mod+Shift+5 move container to workspace number \$workspace5
bindsym \$mod+Shift+6 move container to workspace number \$workspace6
bindsym \$mod+Shift+7 move container to workspace number \$workspace7
bindsym \$mod+Shift+8 move container to workspace number \$workspace8
bindsym \$mod+Shift+9 move container to workspace number \$workspace9
bindsym \$mod+Shift+0 move container to workspace number \$workspace0

for_window [instance="floating"] floating enabled

mode "\$mode_resize" {
	bindsym h resize shrink width 4 px or 4 ppt
	bindsym l resize grow width 4 px or 4 ppt
	bindsym k resize shrink height 4 px or 4 ppt
	bindsym j resize grow height 4 px or 4 ppt
	bindsym Escape mode default
	bindsym Return mode default
}

mode "\$mode_screenshot" {
	bindsym f exec "maim \$(date +%Y%m%d_%H%M%S).png", mode default
	bindsym r exec "maim --select \$(date +%Y%m%d_%H%M%S).png", mode default
	bindsym Escape mode default
	bindsym Return mode default
}

EOF

[ -n "$MPD_ENABLED" ] && cat << EOF
mode "\$mode_mpd" {
	bindsym p exec "mpc toggle", mode default
	bindsym s exec "mpc stop", mode default
	bindsym n exec "mpc next", mode default
	bindsym r exec "mpc prev", mode default
	bindsym Escape mode default
	bindsym Return mode default
}

EOF

cat << EOF
mode "\$mode_volume" {
	bindsym minus exec "wpctl set-volume @DEFAULT_SINK@ 5%-"
	bindsym plus exec "wpctl set-volume @DEFAULT_SINK@ 5%+"
	bindsym m exec "wpctl set-mute @DEFAULT_SINK@ toggle", mode default
	bindsym Escape mode default
	bindsym Return mode default
}

client.background $THEME_BACKGROUND
client.focused $THEME_PRIMARY $THEME_PRIMARY $THEME_ON_PRIMARY $THEME_PRIMARY_VARIANT $THEME_PRIMARY 
client.focused_inactive $THEME_SECONDARY $THEME_SECONDARY $THEME_ON_SECONDARY $THEME_SECONDARY_VARIANT $THEME_SECONDARY
client.unfocused $THEME_SURFACE $THEME_SURFACE $THEME_ON_SURFACE $THEME_PRIMARY $THEME_SURFACE
client.urgent $THEME_WARNING $THEME_WARNING $THEME_ON_WARNING $THEME_PRIMARY $THEME_WARNING

gaps inner 0
gaps outer 0
smart_gaps true
smart_borders no_gaps
hide_edge_borders smart_no_gaps
for_window [all] title_window_icon yes

default_border pixel 4
default_floating_border pixel 4

EOF

