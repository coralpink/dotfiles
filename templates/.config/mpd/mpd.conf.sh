#!/bin/sh

cat << EOF
bind_to_address "$MPD_ADDRESS"

music_directory "$MPD_MUSICDIRECTORY"
db_file "$MPD_DATADIRECTORY/database"
playlist_directory "$MPD_DATADIRECTORY/playlists"
pid_file "$MPD_DATADIRECTORY/pid"
state_file "$MPD_DATADIRECTORY/state"
sticker_file "$MPD_DATADIRECTORY/sticker.sql"

audio_output {
  format "44100:16:2"
  name "fifo"
  path "$MPD_DATADIRECTORY/audio.fifo"
  type "fifo"
}

audio_output {
  name "PipeWire"
  type "pipewire"
}

EOF

