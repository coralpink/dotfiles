#!/bin/sh

cat << EOF
[fonts]
monospace = $FONTS_MONOSPACE_FAMILY:size=$FONTS_MONOSPACE_SIZE;2
sans = $FONTS_SANS_FAMILY:size=$FONTS_SANS_SIZE;2

[colors]
foreground = $THEME_FOREGROUND
background = $THEME_BACKGROUND

primary = $THEME_PRIMARY
primary-variant = $THEME_PRIMARY_VARIANT
on-primary = $THEME_ON_PRIMARY

secondary = $THEME_SECONDARY
secondary-variant = $THEME_SECONDARY_VARIANT
on-secondary = $THEME_ON_SECONDARY

surface = $THEME_SURFACE
on-surface = $THEME_ON_SURFACE

success = $THEME_SUCCESS
on-success = $THEME_ON_SUCCESS

warning = $THEME_WARNING
on-warning = $THEME_ON_WARNING

error = $THEME_ERROR
on-error = $THEME_ON_ERROR

[global]
padding = 1
workspace-padding = 2

[bar/base]
enable-ipc = true
width = 100%
height = 28px
fixed-center = true
font-0 = \${fonts.monospace}
font-1 = \${fonts.sans}
border-color = \${colors.primary}
wm-name = i3
wm-restart = i3
foreground = \${colors.foreground}
background = \${colors.background}

[bar/top]
inherit = bar/base
bottom = false
border-top-size = 1
modules-left = i3 title
modules-center = date
modules-right = wired cpu memory script-pipewire tray

[bar/bottom]
inherit = bar/base
bottom = true
border-bottom-size = 1
modules-left = 
modules-center = 
modules-right = 

[module/i3]
type = internal/i3
index-sort = true
strip-wsnumbers = true
enable-click = true
enable-scroll = false
format = <label-state><label-mode>
label-mode = %mode%
label-mode-padding = \${global.workspace-padding}
label-mode-foreground = \${colors.on-secondary}
label-mode-background = \${colors.secondary}
label-focused = %name%
label-focused-padding = \${global.workspace-padding}
label-focused-foreground = \${colors.on-primary}
label-focused-background = \${colors.primary}
label-unfocused = %name%
label-unfocused-padding = \${global.workspace-padding}
label-unfocused-foreground = \${colors.on-surface}
label-unfocused-background = \${colors.surface}
label-visible = %name%
label-visible-padding = \${global.workspace-padding}
label-visible-foreground = \${colors.on-primary}
label-visible-background = \${colors.primary-variant}
label-urgent = %name%
label-urgent-padding = \${global.workspace-padding}
label-urgent-foreground = \${colors.on-warning}
label-urgent-background = \${colors.warning}

[module/title]
type = internal/xwindow
format = <label>
format-padding = 4
format-prefix = "[window] "
format-prefix-foreground = \${colors.primary}
label = %title:0:128:...%

[module/audio]
type = internal/pulseaudio
interval = 5
use-ui-max = false
format-volume = <label-volume>
format-volume-padding = \${global.padding}
format-volume-prefix = "[volume] "
format-volume-prefix-foreground = \${colors.success}
format-muted = <label-muted>
format-muted-padding = \${global.padding}
format-muted-prefix = "[volume] "
format-muted-prefix-foreground = \${colors.error}
label-volume = %percentage%%
label-muted = "muted"

[module/cpu]
type = internal/cpu
interval = 1
format = <label>
format-padding = \${global.padding}
format-prefix = "[cpu] "
format-prefix-foreground = \${colors.primary}
format-warn = <label-warn>
format-warn-padding = \${global.padding}
format-warn-prefix = "[cpu] "
format-warn-prefix-foreground = \${colors.warning}
label = %percentage:2%%
label-warn = %percentage:2%%

[module/memory]
type = internal/memory
interval = 1
format = <label>
format-padding = \${global.padding}
format-prefix = "[ram] "
format-prefix-foreground = \${colors.primary}
format-warn = <label-warn>
format-warn-padding = \${global.padding}
format-warn-prefix = "[ram] "
format-warn-prefix-foreground = \${colors.warning}
label = %gb_used%
label-warn = %gb_used%

[module/date]
type = internal/date
interval = 1
date = %A, %d.%m.%y
time = %H:%M:%S
format = <label>
format-padding = \${global.padding}
format-prefix = "[date] "
format-prefix-foreground = \${colors.primary}
label = %date% %time%

[module/wired]
type = internal/network
interface-type = wired
format-connected = <label-connected>
format-connected-padding = \${global.padding}
format-connected-prefix = "[network] "
format-connected-prefix-foreground = \${colors.success}
format-disconnected = <label-disconnected>
format-disconnected-padding = \${global.padding}
format-disconnected-prefix = "[network] "
format-disconnected-prefix-foreground = \${colors.error}
label-connected = %local_ip%
label-disconnected = "disconnected"

[module/xkeyboard]
type = internal/xkeyboard
format = <label-layout>
format-padding = \${global.padding}
format-prefix = "[keymap] "
format-prefix-foreground = \${colors.primary}
label-layout = %layout%

[module/tray]
type = internal/tray
format-margin = 8px
tray-size = 16px
tray-spacing = 8px

[module/script-pipewire]
type = custom/script
exec = polybar_pipewire.sh
tail = true
format = <label>
format-padding = \${global.padding}
format-prefix = "[volume] "
format-prefix-foreground = \${colors.primary-variant}
click-left = polybar_pipewire.sh mute %pid%
scroll-up = polybar_pipewire.sh increase %pid%
scroll-down = polybar_pipewire.sh decrease %pid%
; env-SINK = @DEFAULT_SINK@
; env-INTERVAL = 5
; env-INCREMENT = 5
; env-LABEL_OFFLINE = (offline)
; env-LABEL_MUTED = (muted)
; env-LABEL_ERROR = (error)

EOF

