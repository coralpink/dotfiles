#!/bin/sh

if [ "$NEOVIM_MINIMAL" != '1' ]; then
cat << EOF
local paq = require("paq")

paq({
    -- paq
    "savq/paq-nvim",

    -- completion
    "hrsh7th/nvim-cmp",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-vsnip",
    "hrsh7th/vim-vsnip",
    "hrsh7th/cmp-nvim-lsp",
    "neovim/nvim-lspconfig",

    -- directory tree
    "nvim-tree/nvim-tree.lua",

    -- status line
    "nvim-lualine/lualine.nvim",

    -- search
    "nvim-telescope/telescope.nvim",
    "nvim-lua/plenary.nvim",
    "BurntSushi/ripgrep",

    -- syntax
    "nvim-treesitter/nvim-treesitter",

    -- language support
    "jlcrochet/vim-crystal", -- crystal

    -- utils
    "lewis6991/gitsigns.nvim", -- git
    "folke/which-key.nvim", -- key prompt help
    "moll/vim-bbye", -- buffer close
    "tpope/vim-commentary",
    "tpope/vim-surround",
    "petertriho/nvim-scrollbar",
    "kevinhwang91/nvim-bqf",
    "tiagovla/scope.nvim",

    -- themes
    "sainnhe/sonokai",
    "dikiaap/minimalist"
})

-- lua plugins setup
local nvim_tree = require("nvim-tree")
local lualine = require("lualine")
local which_key = require("which-key")
local cmp = require("cmp")
local gitsigns = require("gitsigns")
local scrollbar = require("scrollbar")
local treesitter_config = require("nvim-treesitter.configs")
local scope = require("scope")
local telescope = require("telescope")

nvim_tree.setup({
    reload_on_bufenter = true,
    view = {
        signcolumn = "no",
    },
    renderer = {
        icons = {
            show = {
                file = false,
                folder = false,
                folder_arrow = false,
                git = false,
                modified = false,
                diagnostics = false
            }
        }
    }
})
lualine.setup({
    options = {
        icons_enabled = false
    },
    tabline = {
        lualine_a = {"buffers"},
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {"tabs"}
    }
})
which_key.setup()
scope.setup({})
telescope.setup({
    extensions = {
        "scope",
        "quickfix",
        "nvim-tree",
        "colorscheme"
    },
    pickers = {
        colorscheme = {
            enable_preview = true
        }
    }
})
scrollbar.setup()
cmp.setup({
    snippet = {
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body)
        end,
    },
    window = {},
    mapping = cmp.mapping.preset.insert({
        ["<C-b>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.abort(),
        ["<CR>"] = cmp.mapping.confirm({ select = true })
    }),
    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "vsnip" }
    }, {
        { name = "buffer" }
    })
})
gitsigns.setup({
    signcolumn = true,
    numhl = true,
    current_line_blame = true
})
treesitter_config.setup({
  ensure_installed = {
      "c",
      "cpp",
      "javascript",
      "lua",
      "bash",
      "haskell",
      "nickel",
      "nim",
      "zig"
  },
  sync_install = false,
  auto_install = false,
  highlight = {
    enable = true,
    disable = function(_, buf)
        local max_filesize = 1024 * 1024 -- 1 MiB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,
    additional_vim_regex_highlighting = false
  },
  modules = {},
  ignore_install = {}
})

-- open nvim tree in selected directory on startup
vim.api.nvim_create_autocmd({ "VimEnter" }, {
    callback = function (data)
        local nvim_tree_api = require("nvim-tree.api")

        if vim.fn.isdirectory(data.file) == 1 then
            vim.cmd.cd(data.file)

            nvim_tree_api.tree.open()
        end
    end
})

-- lsp
local lspconfig = require("lspconfig")
local cmp_nvim_lsp = require("cmp_nvim_lsp")
local capabilities = cmp_nvim_lsp.default_capabilities()

-- lua
if vim.fn.executable("lua-language-server") == 1 then
    lspconfig.lua_ls.setup({
        capabilities = capabilities,
        settings = {
            Lua = {
                diagnostics = { globals = { "vim" } },
                runtime = { version = "LuaJIT" },
                workspace = {
                    library = vim.api.nvim_get_runtime_file("", true),
                    checkThirdParty = false
                },
                telemetry = { enable = false }
            }
        }
    })
end

-- c, cpp
lspconfig.clangd.setup({ capabilities = capabilities })

-- crystal
-- lspconfig.crystalline.setup({ capabilities = capabilities })



-- general options
vim.opt.background = "dark"
vim.opt.encoding = "utf-8"
vim.opt.number = true -- show line numbers
vim.opt.autoindent = true -- copy indent from current line when starting a new line
vim.opt.tabstop = 4 -- set tab length
vim.opt.shiftwidth = 4 -- set indent length
vim.opt.expandtab = true -- expand tab to spaces
vim.opt.wrap = false -- no wrap
vim.opt.completeopt = { "menu", "menuone", "noselect" } -- show completion menu
vim.opt.termguicolors = true -- enable 24-bit color palettee
vim.opt.clipboard = "unnamedplus" -- enable system clipboard
vim.opt.cursorline = true -- highlight current line
vim.opt.showtabline = 2 -- always display tabline



-- keybindings

-- map leader to space
vim.g.mapleader = ' '

-- helper function for normal mode mapping
local opts = { silent = true }
local noremap = function(mode, lhs, rhs)
    return vim.keymap.set(mode, lhs, rhs, opts)
end
local nnoremap = function(lhs, rhs)
    return noremap('n', lhs, rhs)
end


-- NvimTree
-- show directory tree
nnoremap("<Leader>do", "<Cmd>NvimTreeFocus<CR>")

-- hide directory tree
nnoremap("<Leader>dq", "<Cmd>NvimTreeClose<CR>")

-- toggle directory tree
nnoremap("<Leader>dd", "<Cmd>NvimTreeToggle<CR>")
nnoremap("<F2>", "<Cmd>NvimTreeToggle<CR>")

-- show current buffer in directory tree
nnoremap("<Leader>dc", "<Cmd>NvimTreeFindFile<CR>")



-- Telescope
-- search file names
nnoremap("<Leader>fs", "<Cmd>Telescope find_files hidden=true<CR>")
nnoremap("<F3>", "<Cmd>Telescope find_files hidden=true<CR>")

-- search all files
nnoremap("<Leader>fg", "<Cmd>Telescope live_grep<CR>")

-- search buffer list
nnoremap("<Leader>fb", "<Cmd>Telescope scope buffers<CR>")
nnoremap("<Leader>f<S-b>", "<Cmd>Telescope buffers<CR>")
nnoremap("<F4>", "<Cmd>Telescope buffers<CR>")

-- search command history
nnoremap("<Leader>fc", "<Cmd>Telescope command_history<CR>")

-- resume
nnoremap("<Leader>ff", "<Cmd>Telescope resume<CR>")



-- Bbye
-- close buffer
nnoremap("<Leader>bd", "<Cmd>Bdelete<CR>")
nnoremap("<Leader>b<S-d>", "<Cmd>Bdelete!<CR>")
nnoremap("<C-q>", "<Cmd>Bdelete<CR>")



-- previous buffer
nnoremap("<Leader>bp", "<Cmd>bprevious<CR>")

-- next buffer
nnoremap("<Leader>bn", "<Cmd>bnext<CR>")
nnoremap("<Leader>bb", "<Cmd>bnext<CR>")

-- previous tab
nnoremap("<Leader>tp", "<Cmd>tabprevious<CR>")

-- next tab
nnoremap("<Leader>tn", "<Cmd>tabnext<CR>")
nnoremap("<Leader>tt", "<Cmd>tabnext<CR>")

-- create tab
nnoremap("<Leader>tc", "<Cmd>tabnew<CR>")

-- close tab
nnoremap("<Leader>tq", "<Cmd>tabclose<CR>")

-- write buffer
nnoremap("<C-s>", "<Cmd>write<CR>")



-- make
nnoremap("<C-m>", "<Cmd>make<CR>")

-- paq
nnoremap("<Leader>p", "<Cmd>PaqSync<CR>")



-- disable arrows
noremap("", "<Up>", "<Nop>")
noremap("", "<Left>", "<Nop>")
noremap("", "<Right>", "<Nop>")
noremap("", "<Down>", "<Nop>")



vim.cmd("colorscheme $NEOVIM_COLORSCHEME")

EOF
else
cat << EOF
local paq = require("paq")

paq({
    "savq/paq-nvim",
    "nvim-tree/nvim-tree.lua",
    "nvim-lualine/lualine.nvim",
    "nvim-telescope/telescope.nvim",
    "nvim-lua/plenary.nvim",
    "BurntSushi/ripgrep",
    "moll/vim-bbye",
    "tpope/vim-commentary",
    "tpope/vim-surround"
})

local nvim_tree = require("nvim-tree")
local lualine = require("lualine")
local telescope = require("telescope")

nvim_tree.setup({
    reload_on_bufenter = true,
    view = {
        signcolumn = "no",
    },
    renderer = {
        icons = {
            show = {
                file = false,
                folder = false,
                folder_arrow = false,
                git = false,
                modified = false,
                diagnostics = false
            }
        }
    }
})
lualine.setup({
    options = {
        icons_enabled = false
    },
    tabline = {
        lualine_a = {"buffers"},
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {"tabs"}
    }
})
telescope.setup({})

vim.opt.background = "dark"
vim.opt.encoding = "utf-8"
vim.opt.number = true
vim.opt.autoindent = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.wrap = false
vim.opt.completeopt = { "menu", "menuone", "noselect" }
vim.opt.termguicolors = true
vim.opt.clipboard = "unnamedplus"
vim.opt.cursorline = true
vim.opt.showtabline = 2

vim.g.mapleader = ' '

local opts = { silent = true }
local noremap = function(mode, lhs, rhs)
    return vim.keymap.set(mode, lhs, rhs, opts)
end
local nnoremap = function(lhs, rhs)
    return noremap('n', lhs, rhs)
end

nnoremap("<Leader>do", "<Cmd>NvimTreeFocus<CR>")
nnoremap("<Leader>dq", "<Cmd>NvimTreeClose<CR>")
nnoremap("<Leader>dd", "<Cmd>NvimTreeToggle<CR>")
nnoremap("<F2>", "<Cmd>NvimTreeToggle<CR>")
nnoremap("<Leader>dc", "<Cmd>NvimTreeFindFile<CR>")

nnoremap("<Leader>fs", "<Cmd>Telescope find_files hidden=true<CR>")
nnoremap("<F3>", "<Cmd>Telescope find_files hidden=true<CR>")
nnoremap("<Leader>fg", "<Cmd>Telescope live_grep<CR>")
nnoremap("<Leader>fb", "<Cmd>Telescope buffers<CR>")
nnoremap("<Leader>f<S-b>", "<Cmd>Telescope buffers<CR>")
nnoremap("<F4>", "<Cmd>Telescope buffers<CR>")

nnoremap("<Leader>fc", "<Cmd>Telescope command_history<CR>")
nnoremap("<Leader>ff", "<Cmd>Telescope resume<CR>")

nnoremap("<Leader>bd", "<Cmd>Bdelete<CR>")
nnoremap("<Leader>b<S-d>", "<Cmd>Bdelete!<CR>")
nnoremap("<C-q>", "<Cmd>Bdelete<CR>")

nnoremap("<Leader>bp", "<Cmd>bprevious<CR>")
nnoremap("<Leader>bn", "<Cmd>bnext<CR>")
nnoremap("<Leader>bb", "<Cmd>bnext<CR>")

nnoremap("<Leader>tp", "<Cmd>tabprevious<CR>")
nnoremap("<Leader>tn", "<Cmd>tabnext<CR>")
nnoremap("<Leader>tt", "<Cmd>tabnext<CR>")
nnoremap("<Leader>tc", "<Cmd>tabnew<CR>")
nnoremap("<Leader>tq", "<Cmd>tabclose<CR>")

nnoremap("<C-s>", "<Cmd>write<CR>")

noremap("", "<Up>", "<Nop>")
noremap("", "<Left>", "<Nop>")
noremap("", "<Right>", "<Nop>")
noremap("", "<Down>", "<Nop>")



vim.cmd("colorscheme $NEOVIM_COLORSCHEME")

EOF
fi

