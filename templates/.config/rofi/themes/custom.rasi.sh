#!/bin/sh

cat << EOF
* {
    text-color: $THEME_FOREGROUND;
    background-color: $THEME_BACKGROUND;
    border-color: $THEME_PRIMARY;
    font: "$FONTS_MONOSPACE_FAMILY $FONTS_MONOSPACE_SIZE";

    primary: $THEME_PRIMARY;
    primary-variant: $THEME_PRIMARY_VARIANT;
    on-primary: $THEME_ON_PRIMARY;

    secondary: $THEME_SECONDARY;
    secondary-variant: $THEME_SECONDARY_VARIANT;
    on-secondary: $THEME_ON_SECONDARY;

    surface: $THEME_SURFACE;
    on-surface: $THEME_ON_SURFACE;

    normal-foreground: $THEME_FOREGROUND;
    normal-background: $THEME_BACKGROUND;

    alternate-normal-foreground: $THEME_FOREGROUND;
    alternate-normal-background: $THEME_BACKGROUND;

    urgent-foreground: $THEME_ON_WARNING;
    urgent-background: $THEME_WARNING;

    alternate-urgent-foreground: $THEME_ON_WARNING;
    alternate-urgent-background: $THEME_WARNING;

    active-foreground: $THEME_ON_SECONDARY;
    active-background: $THEME_SECONDARY;

    alternate-active-foreground: $THEME_ON_SECONDARY;
    alternate-active-background: $THEME_SECONDARY;

    selected-normal-foreground: $THEME_ON_PRIMARY;
    selected-normal-background: $THEME_PRIMARY;

    selected-urgent-foreground: $THEME_ON_PRIMARY;
    selected-urgent-background: $THEME_PRIMARY;

    selected-active-foreground: $THEME_ON_PRIMARY;
    selected-active-background: $THEME_PRIMARY;
}

window {
    background-color: inherit;
    border: 1;
}

inputbar {
    children: [ prompt, entry ];
    background-color: @primary;
    text-color: @on-primary;
    border: 0 0 1px 0;
    border-color: inherit;
}

entry {
    background-color: @surface;
    text-color: @on-surface;
    padding: 8px;
}

prompt {
    background-color: @primary;
    text-color: @on-primary;
    padding: 8px;
}

textbox-prompt-colon {
    expand: false;
    text-color: @normal-foreground;
}

listview {
    scrollbar: true;
}

scrollbar {
    width: 4px;
    background-color: @primary;
    padding: 0;
    handle-color: @primary-variant;
    handle-width: 8px;
}

element {
    padding: 2px;
    border: 0 0 1px 0;
    border-color: @primary;
}

element-text {
    background-color: inherit;
    text-color: inherit;
    vertical-align: 0.5;
}

element-icon {
    size: 1.5em;
    background-color: inherit;
}

message {
    padding: 4px;
}

textbox {
    padding: 4px;
    border: 1px;
    border-color: @primary;
    background-color: @surface;
    text-color: @on-surface;
}

element.normal.normal {
    background-color: @normal-background;
    text-color: @normal-foreground;
}

element.normal.urgent {
    background-color: @urgent-background;
    text-color: @urgent-foreground;
}

element.normal.active {
    background-color: @active-background;
    text-color: @active-foreground;
}

element.selected.normal {
    background-color: @selected-normal-background;
    text-color: @selected-normal-foreground;
}

element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color: @selected-urgent-foreground;
}

element.selected.active {
    background-color: @selected-active-background;
    text-color: @selected-active-foreground;
}

element.alternate.normal {
    background-color: @alternate-normal-background;
    text-color: @alternate-normal-foreground;
}

element.alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color: @alternate-urgent-foreground;
}

element.alternate.active {
    background-color: @alternate-active-background;
    text-color: @alternate-active-foreground;
}
EOF
