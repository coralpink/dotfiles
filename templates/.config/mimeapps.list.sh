#!/bin/sh

cat << EOF
[Default Applications]
application/pdf=$APPLICATIONS_PDFVIEWER.desktop
application/x-www-browser=$APPLICATIONS_WEBBROWSER.desktop
image/gif=$APPLICATIONS_WEBBROWSER.desktop
image/svg+xml=$APPLICATIONS_WEBBROWSER.desktop
x-scheme-handler/http=$APPLICATIONS_WEBBROWSER.desktop
x-scheme-handler/https=$APPLICATIONS_WEBBROWSER.desktop
image/bmp=$APPLICATIONS_IMAGEVIEWER.desktop
image/jpeg=$APPLICATIONS_IMAGEVIEWER.desktop
image/png=$APPLICATIONS_IMAGEVIEWER.desktop
image/webp=$APPLICATIONS_IMAGEVIEWER.desktop
audio/3gpp=$APPLICATIONS_MUSICPLAYER.desktop
audio/aac=$APPLICATIONS_MUSICPLAYER.desktop
audio/flac=$APPLICATIONS_MUSICPLAYER.desktop
audio/mpeg=$APPLICATIONS_MUSICPLAYER.desktop
audio/ogg=$APPLICATIONS_MUSICPLAYER.desktop
audio/opus=$APPLICATIONS_MUSICPLAYER.desktop
audio/wav=$APPLICATIONS_MUSICPLAYER.desktop
audio/wave=$APPLICATIONS_MUSICPLAYER.desktop
audio/webm=$APPLICATIONS_MUSICPLAYER.desktop
video/3gpp=$APPLICATIONS_VIDEOPLAYER.desktop
video/mp4=$APPLICATIONS_VIDEOPLAYER.desktop
video/mpeg=$APPLICATIONS_VIDEOPLAYER.desktop
video/ogg=$APPLICATIONS_VIDEOPLAYER.desktop
video/webm=$APPLICATIONS_VIDEOPLAYER.desktop

EOF

