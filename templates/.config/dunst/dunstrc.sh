#!/bin/sh

cat << EOF
[global]
font = $FONTS_MONOSPACE_FAMILY $FONTS_MONOSPACE_SIZE
gap_size = 4
frame_width = 1
background = "$THEME_BACKGROUND"
foreground = "$THEME_FOREGROUND"
frame_color = "$THEME_PRIMARY"
timeout = 5

[urgency_critical]
background = "$THEME_ERROR"
foreground = "$THEME_ON_ERROR"
frame_color = "$THEME_ON_ERROR"
timeout = 120

EOF

