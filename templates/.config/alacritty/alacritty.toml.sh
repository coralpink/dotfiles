#!/bin/sh

cat << EOF
[colors.bright]
black = "$THEME_BRIGHT_BLACK"
blue = "$THEME_BRIGHT_BLUE"
cyan = "$THEME_BRIGHT_CYAN"
green = "$THEME_BRIGHT_GREEN"
magenta = "$THEME_BRIGHT_MAGENTA"
red = "$THEME_BRIGHT_RED"
white = "$THEME_BRIGHT_WHITE"
yellow = "$THEME_BRIGHT_YELLOW"

[colors.normal]
black = "$THEME_NORMAL_BLACK"
blue = "$THEME_NORMAL_BLUE"
cyan = "$THEME_NORMAL_CYAN"
green = "$THEME_NORMAL_GREEN"
magenta = "$THEME_NORMAL_MAGENTA"
red = "$THEME_NORMAL_RED"
white = "$THEME_NORMAL_WHITE"
yellow = "$THEME_NORMAL_YELLOW"

[colors.primary]
background = "$THEME_BACKGROUND"
foreground = "$THEME_FOREGROUND"

[font]
size = $FONTS_MONOSPACE_SIZE

[font.bold]
family = "$FONTS_MONOSPACE_FAMILY"
style = "Bold"

[font.bold_italic]
family = "$FONTS_MONOSPACE_FAMILY"
style = "Bold Italic"

[font.italic]
family = "$FONTS_MONOSPACE_FAMILY"
style = "Italic"

[font.normal]
family = "$FONTS_MONOSPACE_FAMILY"
style = "Regular"

[[keyboard.bindings]]
action = "SpawnNewInstance"
key = "N"
mods = "Control|Shift"

EOF

