#!/bin/sh

echo '- killing dunst'
pkill -u "$USER" dunst >/dev/null 2>/dev/null

echo '- reloading i3' 
i3-msg reload >/dev/null 2>/dev/null

echo '- restarting polybar'
polybar-msg cmd restart >/dev/null 2>/dev/null

if mpd --kill >/dev/null 2>/dev/null; then
    echo '- restarting mpd'; mpd
fi

paq_path="${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/pack/paqs/start/paq-nvim

if [ ! -d "$paq_path" ]; then
    echo '- downloading paq for neovim'
    git clone --depth=1 'https://github.com/savq/paq-nvim.git' "$paq_path" >/dev/null 2>/dev/null
fi
    
echo '- syncing neovim plugins'
nvim --headless -c 'autocmd User PaqDoneSync qa' -c 'PaqSync' >/dev/null 2>/dev/null

