#!/bin/sh

info() {
    printf "\t\033[0;34m[info]\034[0m " >&2
    echo "$@" >&2
}

notice() {
    printf "\t\033[1;30m[notice]\033[0m " >&2
    echo "$@" >&2
}

warning() {
    printf "\t\033[0;33m[error]\033[0m " >&2
    echo "$@" >&2
}

error() {
    printf "\t\033[0;31m[error]\033[0m " >&2
    echo "$@" >&2
    exit 1
}

