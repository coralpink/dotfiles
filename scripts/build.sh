#!/bin/sh

# shellcheck disable=SC1091
. ./scripts/prelude.sh

template="$1"

shift

cd config || exit 1
for include in "$@"; do
    if ! echo "$include" | grep -q '^config/'; then
        error "unexpected dependency target: $include"
    fi

    # shellcheck disable=SC1090
    . ../"$include"
done
cd .. || exit 1

"$template"

